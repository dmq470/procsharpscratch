﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class ActionAndFuncDelegates
    {
        // This is a target for the Action<> delegate
        static void DisplayMessage( string msg, ConsoleColor txtColor, int printCount )
        {
            // Set color of console text
            var prev = Console.ForegroundColor;

            Console.ForegroundColor = txtColor;

            for ( int i = 0; i < printCount; i++ )
            {
                Console.WriteLine( msg );
            }

            // Restore color.
            Console.ForegroundColor = prev;

            var funcTarget = new Func<int, int, int>( Add );

            var result = funcTarget.Invoke( 40, 40 );

            Console.WriteLine( $"40 + 40 = {result}" );

            Func<int, int, string> funcTarget2 = SumToString;

            var sum = funcTarget2( 90, 300 );

            Console.WriteLine( sum );
        }

        public static void SubMain()
        {
            Console.WriteLine( "** Fun with Action and Func **" );

            // Use the Action<> delegate to point to DisplayMessage
            var actionTarget = new Action<string, ConsoleColor, int>( DisplayMessage );

            actionTarget( "Action Message!", ConsoleColor.Yellow, 5 );

            Console.ReadLine();
        }

        static int Add( int x, int y )
        {
            return x + y;
        }

        static string SumToString( int x, int y )
        {
            return ( x + y ).ToString();
        }
    }
}
