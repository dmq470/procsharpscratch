﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class Boxing
    {
        public static void TestBoxing()
        {
            var arrList = new ArrayList {10, 25, 30};

            Console.WriteLine( "Without explicit cast and unboxing, is there a toString() call??" + arrList[2] );

            Console.WriteLine("Without explicit cast and unboxing, with toString() call??" + arrList[2].ToString() );

            int test = (int) arrList[ 1 ];

            Console.WriteLine( "Unboxed:" + test );

            Console.ReadLine();
        }

    }
}
