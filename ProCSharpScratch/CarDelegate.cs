﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class CarDelegate
    {
        public static void SubMain()
        {
            Console.WriteLine("**** Delegates as event enablersr ****\n");

            // First, make a car object
            var c1 = new Car("SlugBug", 100, 10);

            // Register multiple targets for the notifications
            c1.RegisterWithCarEngine(OnCarEngineEvent);
            c1.RegisterWithCarEngine(OnCarEngineEvent2);

            // Speed up (this will trigger the events).
            Console.WriteLine( "***** Speeding up *****" );
            for ( int i = 0; i < 6; i++ )
                c1.Accelerate( 20 );

            Console.ReadLine();
        }

        private static void OnCarEngineEvent(string msgforcaller)
        {
            Console.WriteLine($"=> { msgforcaller.ToUpper() }");
        }

        private static void OnCarEngineEvent2(string msgForCaller)
        {
            Console.WriteLine("\n**** Message From Car Object ****");
            Console.WriteLine($"=> {msgForCaller}");
            Console.WriteLine("*************************************");
        }

        public class Car
        {
            // Internal state data
            public int CurrentSpeed { get; set; }
            public int MaxSpeed { get; set; }
            public string PetName { get; set; }

            // Is the care alive or dead?
            private bool carIsDead;

            // Class constructors
            public Car() { MaxSpeed = 100; }

            public Car( string name, int maxSp, int currSp )
            {
                CurrentSpeed = currSp;
                MaxSpeed = maxSp;
                PetName = name;
            }

            // 1) Define a delegate name
            public delegate void CarEngineHandler( string msgForCaller );

            // 2) Define a member variable of this delegate
            private CarEngineHandler listOfHandlers;

            // 3) Add registration function for the caller.
            public void RegisterWithCarEngine( CarEngineHandler methodToCall )
            {
                if ( listOfHandlers == null )
                    listOfHandlers = methodToCall;
                else
                    listOfHandlers += methodToCall;
            }

            // 4) Implement the Accelerate() method to invoke the delegate's invocation list under the correct circumstances
            public void Accelerate( int delta )
            {
                // If this car is "dead", send dead message
                if ( carIsDead )
                {
                    listOfHandlers?.Invoke( "Sorry, this car is dead" );
                }
                else
                {
                    CurrentSpeed += delta;

                    // Is this care "almost dead"?
                    if ( 10 == ( MaxSpeed - CurrentSpeed ) )
                    {
                        listOfHandlers?.Invoke( "Careful! Gonna blow!" );
                    }

                    if ( CurrentSpeed >= MaxSpeed )
                        carIsDead = true;
                    else
                        Console.WriteLine( $"CurrentSpeed {CurrentSpeed}" );
                }
            }
        }
    }
}
