﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class CarEvents
    {
        public static void SubMain()
        {
            Console.WriteLine("**** Fun with Events ****\n");

            // First, make a car object
            var c1 = new Car("SlugBug", 100, 10);

            // Register event handlers
            c1.AboutToBlow += CarIsAlmostDoomed;
            c1.AboutToBlow += CarAboutToBlow;
            c1.Exploded += CarExploded;

            // Speed up (this will trigger the events).
            Console.WriteLine("***** Speeding up *****");
            for ( int i = 0; i < 6; i++ )
                c1.Accelerate( 20 );

            c1.Exploded -= CarExploded;

            Console.WriteLine("***** Speeding up again *****");
            for ( int i = 0; i < 6; i++ )
                c1.Accelerate( 20 );

            Console.ReadLine();
        }

        private static void CarAboutToBlow( string msg )
        { Console.WriteLine( msg ); }

        private static void CarIsAlmostDoomed( string msg )
        { Console.WriteLine( $"=> Critical Message from Car: {msg}" ); }

        public static void CarExploded( string msg )
        { Console.WriteLine( msg ); }

        public class Car
        {
            // This delegate works in conjunction with the Car's events
            public delegate void CarEngineHandler( string msg );

            // This car can send these events
            public event CarEngineHandler Exploded;
            public event CarEngineHandler AboutToBlow;

           // Internal state data
           public int CurrentSpeed { get; set; }
           public int MaxSpeed { get; set; }
           public string PetName { get; set; }

           // Is the care alive or dead?
           private bool carIsDead;

           // Class constructors
           public Car() { MaxSpeed = 100; }

           public Car(string name, int maxSp, int currSp)
           {
               CurrentSpeed = currSp;
               MaxSpeed = maxSp;
               PetName = name;
           }

           // 2) Define a member variable of this delegate
           private CarEngineHandler listOfHandlers;

           // 3) Add registration function for the caller.
           public void RegisterWithCarEngine(CarEngineHandler methodToCall)
           {
               if (listOfHandlers == null)
                   listOfHandlers = methodToCall;
               else
                   listOfHandlers += methodToCall;
           }

           // 4) Implement the Accelerate() method to invoke the delegate's invocation list under the correct circumstances
           public void Accelerate(int delta)
           {
               // If this car is "dead", send dead message
               if (carIsDead)
               {
                   Exploded?.Invoke( "This car is dead..." );
               }
               else
               {
                   CurrentSpeed += delta;

                   // Is this care "almost dead"?
                   if (10 == (MaxSpeed - CurrentSpeed))
                   {
                       AboutToBlow?.Invoke("Careful! Gonna blow!");
                   }

                   if (CurrentSpeed >= MaxSpeed)
                       carIsDead = true;
                   else
                       Console.WriteLine($"CurrentSpeed {CurrentSpeed}");
               }
           }
        }
    }
}
