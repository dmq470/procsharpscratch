﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class FunWithObservableCollection
    {
        public static void SubMain()
        {
            // Make a collection to observe and add a few Person objects
            var people = new ObservableCollection<Person>()
                         {
                             new Person {FirstName = "Peter", LastName = "Murphy", Age = 52},
                             new Person {FirstName = "Kevin", LastName = "Key", Age = 48}
                         };

            // Wire up the CollectionChanged event
            people.CollectionChanged += PeopleOnCollectionChanged;

            people.Add( new Person( "Fred", "Smith", 32 ) );
            people.RemoveAt( 0 );
        }

        static void PeopleOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            // What was the action that caused the event?
            Console.WriteLine( $"Action for the event: {e.Action}" );

            // Removed smth
            if ( e.Action == NotifyCollectionChangedAction.Remove )
            {
                Console.WriteLine( "Here are the OLD items: " );
                foreach ( Person p in e.OldItems )
                {
                    Console.WriteLine( p.ToString() );
                }

                Console.WriteLine();
            }

            // Added smth
            if ( e.Action == NotifyCollectionChangedAction.Add )
            {
                // Now show the new items that were inserted 
                Console.WriteLine( "Here are the NEW items: " );
                foreach ( Person p in e.NewItems )
                {
                    Console.WriteLine( p.ToString() );
                }
            }

        }
    }

    public class Person
    {
        public int Age { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Person() { }

        public Person( string firstName, string lastName, int age )
        {
            Age = age;
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"Name: {FirstName} {LastName}, Age: {Age}";
        }
    }
}
