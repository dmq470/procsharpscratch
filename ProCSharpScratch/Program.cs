﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Boxing.TestBoxing();
            //FunWithObservableCollection.SubMain();
            //SimpleDelegate.SubMain();
            //ActionAndFuncDelegates.SubMain(  );
            //CarEvents.SubMain();
            SimpleLambdaExpressions.SubMain();
        }

        public static void SwapStrings( string s1, string s2 )
        {
            var tempStr = s1;
            s1 = s2;
            s2 = tempStr;
        }

        public static void testSwapStrings()
        {
            Console.WriteLine("methods");

            var str1 = "Flip";
            var str2 = "Flop";

            Console.WriteLine("Before: {0}, {1}", str1, str2);

            SwapStrings(str1, str2);

            Console.WriteLine("After: {0}, {1}", str1, str2);

            Console.ReadLine();
        }

        struct MyStruct
        {
             
        }
    }
}
