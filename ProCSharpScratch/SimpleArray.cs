﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    class SimpleArray
    {
        static void SimpleArrays()
        {
            Console.WriteLine( "=> Simple Array Creation." );

            // Create an array of ints containing 3 elements indexed 0,1,2

            var myInts = new int[3];

            // Create a 100 item string array, indexed 0 - 99

            var booksOnDotNet = new string[100];


        }
    }
}
