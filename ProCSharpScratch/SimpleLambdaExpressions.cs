﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCSharpScratch
{
    public class SimpleLambdaExpressions
    {
        public static void SubMain()
        {
            // Make a list of integers
            var list = new List<int>();

            list.AddRange( new int[] {20, 1, 4, 8, 9, 44} );

            // Call FindAll() using traditional delegate syntax
            var callback = new Predicate<int>( IsEvenNumber );
            var evens = list.FindAll( callback );
            var evens2 = list.FindAll( i => i % 2 == 0 );

            Console.WriteLine( "Here are the even nums: " );
            foreach ( var even in evens )
            {
                Console.WriteLine( $"{even}\t" );
            }

            Console.ReadLine();
        }

        private static bool IsEvenNumber( int num )
        {
            return num % 2 == 0;
        }

        public void TraditionalDelegateSyntax()
        {

        }
    }
}
