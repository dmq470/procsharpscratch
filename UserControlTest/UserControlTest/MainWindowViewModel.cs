﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using UserControlTest.Annotations;

namespace UserControlTest
{
    public class MainWindowViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _textBoxValueProp;

        public MainWindowViewModel()
        {
            TextBoxValueProp = "";
        }


        public string TextBoxValueProp
        {
            get { return _textBoxValueProp; }
            set
            {
                _textBoxValueProp = value;
                OnPropertyChanged(TextBoxValueProp);
            }   
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string this[string columnName]
        {
            get
            {
                if ( columnName == nameof(TextBoxValueProp) )
                     return  TextBoxValueProp.Length > 0 ? "" : "Needs to be populated";
                return "";
            }
        }

        public string Error { get; }
    }
}
