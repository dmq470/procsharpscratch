﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControlTest
{
    /// <summary>
    /// Interaction logic for ValidatingUserControl.xaml
    /// </summary>
    public partial class ValidatingUserControl: IDataErrorInfo
    {
        public ValidatingUserControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty TextBoxValueProperty = DependencyProperty.Register(
            "TextBoxValue", typeof(string), typeof(ValidatingUserControl), new PropertyMetadata(default(string)));

        public string TextBoxValue
        {
            get { return (string) GetValue(TextBoxValueProperty); }
            set { SetValue(TextBoxValueProperty, value); }
        }

        public string this[string columnName] => Validation.GetHasError(this) ? "has error" : "";

        public string Error { get; }
    }
}
